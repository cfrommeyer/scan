#!/bin/bash

# Programs
SCANIMAGE=scanimage
UNPAPER=unpaper
TESSERACT=tesseract
PDFTK=pdftk
PNMFLIP=pnmflip

# Constants
DUPLEX_FEEDER='Automatic Document Feeder(centrally aligned,Duplex)'
SIMPLEX_FEEDER='Automatic Document Feeder(centrally aligned)'
DEVICE='brother4:net1;dev0'
MODE='24bit Color[Fast]'

CONFIG_FILE="${HOME}/.config/scanrc"
if [[ -f "$CONFIG_FILE" ]]; then
  echo "Reading configuration from $CONFIG_FILE"
  . "$CONFIG_FILE"
fi

# Variables
RESOLUTION=300
SOURCE="$DUPLEX_FEEDER"
LEFT=0
TOP=0
X=215.88
Y=297

PREFIX=$(date +%Y-%m-%d_%H-%M-%S)

FILE_PATTERN="${PREFIX}_%03d.pnm"
OCR_FILE_PATTERN="un-${PREFIX}_*."
ROTATE=0

CLEANUP_FILES='yes'

usage() {
  echo "Usage: scan.sh [-k|--keep] [-u|--unpaper] [-s|--simplex] [-o|--one-page] [-d|--device <DEVICE>] [--a5]"
  echo "               [-r|--rotate <angle>]"
  echo "----"
  echo "  -k, --keep      Keep intermediate files"
  echo "  -u, --unpaper   Use unpaper(1) to prepare scanned files before OCR processing"
  echo "  -s, --simplex   Scan in simplex mode"
  echo "  -o, --one-page  Scan only one page"
  echo "  -d, --device    Use <DEVICE> for scanning instead default"
  echo "  --a5            Use a5 paper size"
  echo "  -r, --rotate    Rotate the scanned page by 90° clockwise (angle=90) or counter-clockwise (angle=-90)"
  echo
}

parse_options() {
  PARSED_OPTIONS=$(getopt -o 'kuosd:r:' --long 'keep,unpaper,one-page,simplex,device:,a5,rotate:' -n 'scan' -- "$@")
  local EXIT_CODE=$?
  if [[ $EXIT_CODE -ne 0 ]]; then
    echo "GetOpt terminated with exit code: $EXIT_CODE"
    usage
    exit 1
  fi
}

process_options() {
  while true; do
    case "$1" in
      '-k'|'--keep')
        echo "Keeping all intermediate files"
        unset CLEANUP_FILES
        shift
        ;;
      '-u'|'--unpaper')
        echo "Using Unpaper"
        UNPAPER_FILES='yes'
        shift
        ;;
      '-s'|'--simplex')
        echo "Simplex scan"
	SOURCE="$SIMPLEX_FEEDER"
	shift
	;;
      '-o'|'--one-page')
        echo "Scanning one page only"
        ONE_PAGE_SCAN='yes'
        shift
        ;;
      '-d'|'--device')
        echo "Using device $2"
	DEVICE="$2"
	shift
	shift
	;;
      '--a5')
	echo "Using A5 paper size"
        Y=148.5
	ROTATE=$(($ROTATE + 1))
	shift
	;;
      '-r'|'--rotate')
	echo "Rotate page by $2"
	if [[ "$2" == "90" ]]; then
	  ROTATE=$(($ROTATE + 1))
	elif [[ "$2" == "-90" ]]; then
	  ROTATE=$(($ROTATE - 1))
	fi
	shift
	shift
	;;
      '--')
        shift
        break;
        ;;
      *)
        echo "Error in option parsing. Found unexpected $1"
        usage
        exit 1
        ;;
    esac
  done
}

error() {
  echo -e "\e[31m$@\e[0m"
}

step() {
  echo -e "\e[34m$@\e[0m"
}

success() {
  echo -e "\e[32m$@\e[0m"
}

check_binary() {
  echo "Checking for $1..."
  if [[ ! -x $(which $1) ]]; then
    error "Missing $1 binary... aborting."
    exit 1
  fi
}

parse_options "$@" || exit $?
eval process_options $PARSED_OPTIONS || exit $?
unset PARSED_OPTIONS

step "Checking for required binaries..."
check_binary $SCANIMAGE || exit 10
check_binary $UNPAPER || exit 11
check_binary $TESSERACT || exit 12
check_binary $PDFTK || exit 13
check_binary $PNMFLIP || exit 14

step "Scanning ..."
if [[ $ONE_PAGE_SCAN ]]; then
  $SCANIMAGE -d "$DEVICE" \
      --mode "$MODE" \
      --resolution $RESOLUTION \
      -l $LEFT -t $TOP -x $X -y $Y > ${PREFIX}_001.pnm
else
  $SCANIMAGE -d "$DEVICE" --source "$SOURCE" \
      --batch="$FILE_PATTERN" \
      --mode "$MODE" \
      --resolution $RESOLUTION \
      -l $LEFT -t $TOP -x $X -y $Y
fi
if [[ $? -ne 0 ]]; then
	error "Error while scanning"
	exit 1
fi

if [[ "$ROTATE" == "0" ]]; then
  ROTATE=""
elif [[ "$ROTATE" == "1" ]]; then
  ROTATE="-rotate270"
elif [[ "$ROTATE" == "-1" ]]; then
  ROTATE="-rotate90"
elif [[ "$ROTATE" == "-2" || "$ROTATE" == "2" ]]; then
  ROTATE="-rotate180"
fi
if [[ -n "$ROTATE" ]]; then
  echo -n "Rotating scanned pages with pnmflip $ROTATE..."
  for file in ${PREFIX}*.pnm; do
    $PNMFLIP $ROTATE "$file" > "rot-$file"
    rm "$file"
    mv "rot-$file" "$file"
  done
  echo "done."
fi

if [[ $UNPAPER_FILES ]]; then
  step "Improving scanned files..."
  $UNPAPER --no-grayfilter $FILE_PATTERN un-$FILE_PATTERN
  if [[ $? -ne 0 ]]; then
    error "Error stripping paper from scans"
    exit 2
  fi
else
  OCR_FILE_PATTERN="${PREFIX}_*."
fi

step "OCR Processing..."
for i in ${OCR_FILE_PATTERN}pnm; do
  $TESSERACT --dpi $RESOLUTION $i $(basename -s.pnm $i) pdf
  if [[ $? -ne 0 ]]; then
    error "Error in OCR processing $i."
    echo "Will keep all intermediate files for manual processing. Still continuing..."
    unset CLEANUP_FILES
  fi
done

step "Combining PDF..."
$PDFTK ${OCR_FILE_PATTERN}pdf cat output $PREFIX.pdf
if [[ $? -ne 0 ]]; then
  error "Error concatenating PDFs."
  exit 4
fi

if [[ $CLEANUP_FILES ]]; then
  step "Cleaning up..."
  rm ${PREFIX}_* un-${PREFIX}*
fi

success "DONE."

